##id is used for localization
#example_law = {
#	#tooltip description for the law
#	tooltip = "loc_key"
#	
#	#gfx entry & frame for law entry icon
#	#gfx entry can be omitted, in that case it will default to GFX_occupation_policy_icon_strip
#	icon = "GFX_icon:5"
#	
#	#plays when the law is selected
#	sound_effect = "sound_effect" 
#	
#	#if a law is not visible, it won't show up in gui at all
#	#scope is occupier country, occupied country is not set (this is only check at country level, not target level)
#	visible = {
#		always = yes
#	}
#
#	#if a law is not available, it will show up in disable state ingui and won't be selectable
#	#scope is occupier country and from scope is occupied country
#	available = {
#		always = yes
#	}
#
#	#if a current modifier is no longer active (visible/available is false) it will fallback to this law
#	fallback_law = law_id
#	
#	#main fallback modifier will be used when previous modifier is no longer active and there is no fallback_law, there must be exactly one fallback modifier
#	#if you are lacking manpower/equipments for your current laws, fallback modifier bonuses will apply instead (will lerp to fallback and at 0 manpower you will be forced to switch)
#	main_fallback_law = yes
#	
#	#applies to states if the law is selected
#	state_modifier = {
#	
#	}
#	
#	#states with resistance supressed (at 0) will get this modifier instead (if defined)
#	suppressed_state_modifier = {
#		
#	}
#	
#	#by default laws are sorted by load order, gui_order can be used to reorder them in gui. by default it is 0
#	gui_order = 1
#
#	# score used for selecting a law for state
#	# scope is state
#	# from is occupier
#	# from from is occupied
#	# ai_desire_resource temp variables will be available for the triggers, 
#	# which is a value in between 0-1 to show desire for a certain resource 
#	# uncapped_resistance_target temp variable can be used for getting resistance that that is not capped to 0-100
#	# resistance_target_without_law temp variable can be used for accessing resistance target value that is not modifier by law
#	# garrison_min_support_ratio garrison_equipment_support_ratio garrison_manpower_support_ratio gives what ratio of the garrison factor we can support for current template and buffers
#	ai_will_do = {
#		base = 100
#		
#		modifier = {
#			steel > 10
#			add = 100
#		}
#	}
#}


no_garrison = {
	icon = 1
	default_law = yes
	sound_effect = "Martial_Law_Interface_No_Garrison"
	
	state_modifier = {
		resistance_target = 0.4
		no_compliance_gain = 1
		required_garrison_factor = -1
	}
	
	suppressed_state_modifier = {
	}
}

autonomous_occupation = {
	icon = 2
	sound_effect = "Martial_Law_Interface_Autonomous_Occupation_Unique_DEM"
	
	visible = {
		has_government = democratic #? on design
	}
	
	state_modifier = {
	}
}

foreign_civilian_oversight = {
	icon = 3
	sound_effect = "Martial_Law_Interface_Foreign_Cvilian_Oversight"
	
	state_modifier = {
		compliance_gain 				= 0.000
		local_factories 				= 0.00
		local_resources 				= 0.00
	}
	starting_law = yes	
}


local_police_force_garrison = {
	icon = 11
	sound_effect = "Martial_Law_Interface_Local_Police_Force_Garrison"
		
	state_modifier = {
		local_factories 				= 0.00
		local_resources 				= 0.00
	}
}

secret_police_oversight = {
	icon = 4
	sound_effect = "Martial_Law_Interface_Secret_Police_Oversight"
	
	state_modifier = {
		local_factories 				= 0.05
		local_resources 				= 0.05
		enemy_operative_detection_chance_over_occupied_tag = 1 # offset detection chance by +1%
		enemy_intel_network_gain_factor_over_occupied_tag = -0.25 # -25% scaled by the number of occupied states and the ratio of the network covering those occupied states
		# todo + spy defense
	}
}

liberate_workers_occupation = {
	icon = 5
	sound_effect = "Martial_Law_Interface_Liberate_Workers_Occupation_Unique_COM"
	
	visible = {
		has_government = communism
	}
	
	state_modifier = {
		local_factories 				= 0.2
		local_resources 				= 0.3
	}
	
	
}

military_governor_occupation = {
	icon = 6
	sound_effect = "Martial_Law_Interface_Military_Governor_Occupation"
	
	state_modifier = {
		#local_factories 			= 0.00
		local_resources 			= 0.10
		local_manpower 				= 0.08
	}
}

martial_law_occupation = {
	icon = 7
	sound_effect = "Martial_Law_Interface_Martial_Law_Occupation_Default"
	
	state_modifier = {	
		#required_garrison_factor 		= 0.0
		#resistance_damage_to_garrison 	= 0.0		
		#local_factories 				= 0.0
		local_resources 				= 0.1
	}
}

forced_labor_occupation = {
	icon = 8
	sound_effect = "Martial_Law_Interface_Forced_Labor_Occupation"
	
	state_modifier = {	
		local_resources 					= 0.40
		local_factories 					= 0.05
		repair_speed_infrastructure_factor 	= 0.25
		
		custom_modifier_tooltip = "resource_sabotage_decrease_tooltip"
	}
}

harsh_quotas_occupation = {
	icon = 9
	sound_effect = "Martial_Law_Interface_Harsh_Quotas_Occupation"
	
	state_modifier = {	
		local_factories 				= 0.25
		local_resources 				= 0.05
		repair_speed_arms_factory_factor = 0.25
		repair_speed_industrial_complex_factor = 0.25
		
		custom_modifier_tooltip = "factory_sabotage_decrease_tooltip"
	}
}

brutally_oppressive_occupation = {
	icon = 10
	sound_effect = "Martial_Law_Interface_Brutally_Oppressive_Occupation_Unique_FAS"
	
	visible = {
		has_government = fascism
	}
	
	state_modifier = {		
		local_resources 				= 0.10
	}
}
