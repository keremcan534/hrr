version="1.0.2"
tags={
	"Balance"
	"Fixes"
	"Gameplay"
	"Graphics"
}
name="Operation Overlord: Historical Multiplayer Mod"
picture="thumbnail.png"
replace_path="map/strategicregions"
replace_path="history/states"
replace_path="history/countries"
replace_path="history/units"
supported_version="1.9.3"
remote_file_id="2064824700"