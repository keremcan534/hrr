
state={
	id=195
	name="STATE_195"

	history={
		owner = SOV
		victory_points = {
			3151 30.0 
		}
		buildings = {
			infrastructure = 7
			arms_factory = 8
			industrial_complex = 4
			dockyard = 3
			air_base = 10
			13258 = {
				naval_base = 10
				coastal_bunker = 5

			}

		}
		add_core_of = SOV
		1939.1.1 = {
			buildings = {
				arms_factory = 7

			}

		}

	}

	provinces={
		79 149 3060 3151 6174 9097 9150 9164 9197 11068 11080 13258 13259 13260 13261 13262 
	}
	manpower=3750158
	buildings_max_level_factor=1.000
	state_category=metropolis
}
